using Aforo255.Cross.Discovery.Consul;
using Aforo255.Cross.Log.Src.Elastic;
using Aforo255.Cross.Token.Src;
using Aforo255.Cross.Tracing.Src.Zipkin;
using Microsoft.EntityFrameworkCore;
using MS.AFORO255.Security.Data;
using MS.AFORO255.Security.Persistences;
using MS.AFORO255.Security.Services;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

builder.Host.ConfigureAppConfiguration((host, builder) =>
{
    var c = builder.Build();
    builder.AddNacosConfiguration(c.GetSection("nacosConfig"));
});

ExtensionsElastic.ConfigureLog(builder.Configuration);
builder.WebHost.UseSerilog();

builder.Services.AddControllers();
builder.Services.AddDbContext<ContextDatabase>(
    opt =>
    {
        opt.UseMySQL(builder.Configuration["cn:mysql"]);
    });
builder.Services.AddScoped<IAccessService, AccessService>();
builder.Services.Configure<JwtOptions>(builder.Configuration.GetSection("jwt"));
builder.Services.AddConsul();
builder.Services.AddJZipkin();

var app = builder.Build();
app.UseAuthorization();
app.MapControllers();
app.UseConsul();
DbCreated.CreateDbIfNotExists(app);
app.Run();